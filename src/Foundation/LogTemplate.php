<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is class for return the json log template formatting
 */

namespace aqsat\helper\Foundation;


final class LogTemplate {


    /**
     * @param string $file
     * @param int $line
     * @param string $message
     * @return void
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @Description This is function for return the success response template
     */


     public static function error(string $file , int $line , string $message , string $event , array $extra = [])
     {

         $data =  [
              'file' => $file,
              'line' => $line,
          ];

         if ($extra) {

             $data['extra'] = $extra;
         }

         activity()->setEvent($event)->withProperties($data)->log($message);

     }


 }
