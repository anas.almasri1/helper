<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is class for return the json template formatting
 */

namespace aqsat\helper\Foundation;

use Illuminate\Http\JsonResponse;
use aqsat\helper\Constants\ResponseStatus;

final class ResponseTemplate {


     /**
      * @param int $status_code
      * @param string|null $message
      * @param array|object $data
      * @return JsonResponse
      * @author Anas almasri (anas.almasri@hayperpay.com)
      * @Description This is function for return the success response template
      */


     public static function success(int $status_code , string $message = null , array|object $data = [] ): JsonResponse
     {


         return response()->json([

             'status_code' => $status_code,
             'message' => $message,
             'data' => $data,
             'status' => ResponseStatus::SUCCESS
         ]);

     }


     /**
      * @param int $status_code
      * @param string|null $message
      * @param array|object $errors
      * @return JsonResponse
      * @author Anas almasri (anas.almasri@hayperpay.com)
      * @Description This is function for return the success response template
      */


     public static function error(int $status_code , string $message = null , array|object $errors = [] ): JsonResponse
     {
         return response()->json([

             'status_code' => $status_code,
             'message' => $message,
             'errors' => $errors,
             'status' => ResponseStatus::ERROR
         ]);

     }


    /**
     * @param int $status_code
     * @param string|null $message
     * @param array|object $data
     * @return JsonResponse
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @Description This is function for return the success response template
     */


    public static function pagination(int $status_code ,  int $limit , int $page , int $total , string $message = null , array|object $data = [] ): JsonResponse
    {


        return response()->json([

            'status_code' => $status_code,
            'total' => $total,
            'limit' => $limit,
            'page' => $page,
            'message' => $message,
            'data' => $data,
            'status' => ResponseStatus::SUCCESS
        ]);

    }


 }
