<?php

namespace aqsat\helper\Console\Commands;

use aqsat\helper\Models\City;
use aqsat\helper\Facade\Helper;
use Illuminate\Console\Command;
use aqsat\helper\Models\Country;
use aqsat\helper\Foundation\LogTemplate;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;

class FetchCountriesData extends Command {


    protected $signature = 'aqsat:fetch-countries-list';

    protected $description = 'This command line for fetch the countries list from API to our DB';


    public function handle(){

        try {

            $response = Helper::country()->list();

            $response = json_decode($response->getBody()->getContents() , true);

            $i = 1 ;

            $count = count($response['data']);

            foreach ($response['data'] as $item) {

                $query = Country::findWhere(['iso2' => $item['iso2']]);

                if ($query) {

                    $this->error('This item is exist already');

                    continue;
                }

                $data = [

                    'name' => $item['country'],
                    'order' => $i,
                    'iso2' => $item['iso2'],
                    'iso3' => $item['iso3'],
                    'status' => true
                ];

               $country = Country::createSelf($data);

               $j = 1;

               foreach ($item['cities'] as $city) {

                   City::createSelf([

                       'name' => $city,
                       'country_uuid' => $country->uuid,
                       'order' => $j,
                       'status' => true
                   ]);

                   $this->warn($i . '-' . $count . ' ' . $city . ' was been created');

                   $j++;
               }

                $this->info($i . '-' . $count . ' ' . $item['country'] . ' was been created');

                $i++;
            }

        }catch (ClientException | ServerException $exception) {

            LogTemplate::error(
                $exception->getFile() ,
                $exception->getLine() ,
                $exception->getMessage() ,
                __CLASS__ . '--' . __FUNCTION__
            );

        }

    }

}
