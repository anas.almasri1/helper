<?php

use Illuminate\Support\Facades\Route;
use aqsat\helper\Http\Controllers\CityController;
use aqsat\helper\Http\Controllers\CountryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::controller(CountryController::class)->name('country.')->prefix('/country')->group(function (){


    Route::get('/' , 'index')->name('index');

});

Route::controller(CityController::class)->name('city.')->prefix('/city')->group(function (){


    Route::get('/' , 'index')->name('index');

});
