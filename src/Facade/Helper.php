<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is facade class for Helper packages
 */


namespace aqsat\helper\Facade;


use Illuminate\Support\Facades\Facade;

class Helper extends Facade{

    /***************************************************************
     *
     * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
     * @Description: Get the registered name of the component.
     *
     ***************************************************************
     */
    protected static function getFacadeAccessor(){

        return 'Helper';
    }
}
