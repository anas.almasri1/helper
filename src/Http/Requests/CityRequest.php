<?php

/***************************************************************
 *
 * @Original_Author: Anas almasri (anas.almasri@hayperpay.com)
 * @Description: to check city list validation
 *
 ***************************************************************
 */


namespace aqsat\helper\Http\Requests;


use aqsat\helper\Rules\CountryExist;

class CityRequest extends BaseRequest {

    public function rules(){

        $rules = [];

        $rules['country_uuid'] = ['required' , 'uuid' , new CountryExist()];

        return $rules;

    }
}
