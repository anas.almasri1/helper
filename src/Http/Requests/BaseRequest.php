<?php

/***************************************************************
 *
 * @Original_Author: Anas almasri (anas.almasri@hayperpay.con)
 * @Description: This Base Request
 *
 ***************************************************************
 */

namespace aqsat\helper\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest {


    public function authorize(){

        return true;
    }


}
