<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is resource class for return city data
 */

namespace aqsat\helper\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property string $uuid
 * @property string $name
 */

class CityResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'name' => $this->name
        ];
    }
}
