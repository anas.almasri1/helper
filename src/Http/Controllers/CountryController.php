<?php


namespace aqsat\helper\Http\Controllers;

use Illuminate\Http\Response;
use aqsat\helper\Models\Country;
use aqsat\helper\Foundation\ResponseTemplate;
use aqsat\helper\Http\Resources\CountryResource;

class CountryController extends BaseController {


    /**
     * @return mixed
     * ||************************* API Reference *********************************||
     * @uses  This API for get list of countries
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @version V1
     * @endpoint /api/aqsat/helper/v1/country
     * @method GET
     * ||************************* API Reference *********************************||
     */

     public function index(){

        $limit = $this->getLimit();

        $page = $this->getPage();

        $country = Country::getPaginate(['status' => true] , $limit);

        return ResponseTemplate::pagination(
            Response::HTTP_OK ,
            $limit ,
            $page ,
            $country->total() ,
            trans('helper::response.data_retrieved_successfully'),
            CountryResource::collection($country)
        );

     }



}
