<?php

namespace aqsat\helper\Http\Controllers;

use aqsat\helper\Constants\Request;
use App\Http\Controllers\Controller;
use aqsat\helper\Constants\LanguageCode;

class BaseController extends Controller {

    public function __construct(){



        if (request()->hasHeader('Accept-Language') && in_array(request()->header('Accept-Language') , LanguageCode::LIST)) {

            app()->setLocale(request()->header('Accept-Language'));

        }

    }

    final public function getLimit(){

       $limit =  request()->has('limit') ? request()->get('limit') : Request::MAX_LIMIT;

       if ($limit > Request::MAX_LIMIT || $limit < Request::MIN_LIMIT) {

           $limit = Request::MAX_LIMIT;
       }

       return $limit;

    }


    final public function getPage(){

      return request()->has('page') ? request()->get('page') : Request::DEFAULT_PAGE;

    }


}
