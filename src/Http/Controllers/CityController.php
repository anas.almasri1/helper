<?php


namespace aqsat\helper\Http\Controllers;

use Illuminate\Http\Response;
use aqsat\helper\Models\City;
use aqsat\helper\Http\Requests\CityRequest;
use aqsat\helper\Foundation\ResponseTemplate;
use aqsat\helper\Http\Resources\CityResource;

class CityController extends BaseController
{

    /**
     * @return mixed
     * ||************************* API Reference *********************************||
     * @uses  This API for get list of cities depends on country id
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @version V1
     * @endpoint /api/aqsat/helper/v1/city
     * @method GET
     * ||************************* API Reference *********************************||
     */

    public function index(CityRequest $request)
    {

        $data = $request->only('country_uuid');

        $limit = $this->getLimit();

        $page = $this->getPage();

        $city = City::getPaginate(['country_uuid' => $data['country_uuid'] , 'status' => true], $limit);

        return ResponseTemplate::pagination(
            Response::HTTP_OK,
            $limit,
            $page,
            $city->total(),
            trans('helper::response.data_retrieved_successfully'),
            CityResource::collection($city)
        );

    }

}
