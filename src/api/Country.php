<?php

namespace aqsat\helper\api;

use Psr\Http\Message\ResponseInterface;

class Country{


    private array $endpoint = [

        'countries'=>'countries',
    ];


    private mixed $timeOut;


    private mixed $debug;


    final public function __construct($debug = false , $timeOut = 30){

        $this->debug = $debug ;

        $this->timeOut = $timeOut;
    }


    final public function list(): ResponseInterface
    {

        $endpoint = $this->endpoint['countries'];

        $parameter = [

            'headers'=>[
                'Content-Type'     => 'application/json',
                'Accept'           => 'application/json'
            ]
        ];

        return $this->Api()->setEndPoint($endpoint)->setOptions($parameter)->get();
    }

    private function Api(){

        $base_url = config('helper.country_base_url').'/api/v0.1/';

        return new BaseGateway( $base_url , $this->debug , $this->timeOut );
    }


    final public static function object($debug = true , $timeOut = 30){

        return new self( $debug , $timeOut );
    }
}
