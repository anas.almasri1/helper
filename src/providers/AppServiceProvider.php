<?php

namespace aqsat\helper\providers;

use aqsat\helper\api\Main;
use Illuminate\Support\ServiceProvider;
use aqsat\helper\Console\Commands\FetchCountriesData;

class AppServiceProvider extends ServiceProvider{

    public function register() {

        if($this->app->runningInConsole()){

            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

            $this->commands([
                FetchCountriesData::class
            ]);
        }


        $this->registerConfig();

    }


    public function boot() {

        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'helper');

    }

    protected function registerConfig(){

        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('Api.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'helper'
        );
        $this->app->singleton('Helper', static function () {

            return new Main();
        });
    }
}
