<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is class for status code constants
 */

namespace aqsat\helper\Constants;


 final class LanguageCode {


    public const EN = 'en';

    public const AR = 'ar';

    public const LIST = [

        self::EN,

        self::AR
    ];




 }
