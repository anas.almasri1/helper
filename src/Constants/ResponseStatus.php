<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is class for status code constants
 */

namespace aqsat\helper\Constants;


 final class ResponseStatus {


    public const SUCCESS = 1 ;

    public const ERROR = 2 ;

 }
