<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: This is class for status code constants
 */

namespace aqsat\helper\Constants;


 final class Request {


    public const MAX_LIMIT = 150 ;

    public const MIN_LIMIT = 1 ;

    public const DEFAULT_PAGE = 1;

 }
