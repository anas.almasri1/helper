<?php

namespace aqsat\helper\Observers;


use Illuminate\Support\Str;
use aqsat\helper\Models\City;

class CityObserver
{


    public function updated(City $model)
    {
    }

    public function updating(City $model)
    {
    }


    public function created(City $model)
    {

    }


    public function creating(City $model)
    {

        $model->uuid = Str::uuid()->toString();

    }


    public function deleted(City $model)
    {
    }


    public function deleting(City $model)
    {
    }

    function restored(City $model)
    {
    }


    public function restoring(City $model)
    {
    }
}
