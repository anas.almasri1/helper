<?php

namespace aqsat\helper\Observers;


use Illuminate\Support\Str;
use aqsat\helper\Models\Country;

class CountryObserver
{


    public function updated(Country $model)
    {
    }

    public function updating(Country $model)
    {
    }


    public function created(Country $model)
    {

    }


    public function creating(Country $model)
    {

        $model->uuid = Str::uuid()->toString();

    }


    public function deleted(Country $model)
    {
    }


    public function deleting(Country $model)
    {
    }

    function restored(Country $model)
    {
    }


    public function restoring(Country $model)
    {
    }
}
