<?php

namespace aqsat\helper\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckPhoneRule implements Rule{

    /**
     * Determine if the validation rule passes.
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes( $attribute , $value ){

        return preg_match('/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/' , $value);

    }

    /**
     * Get the validation error message.
     * @return string
     */
    public function message(){

        return trans('helper::response.invalid_phone_format');

    }
}
