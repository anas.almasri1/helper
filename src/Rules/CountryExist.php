<?php

namespace aqsat\helper\Rules;

use aqsat\helper\Models\Country;
use Illuminate\Contracts\Validation\Rule;

class CountryExist implements Rule{

    /**
     * Determine if the validation rule passes.
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes( $attribute , $value ){

        return !empty(Country::findWhere(['uuid' => $value]));
    }

    /**
     * Get the validation error message.
     * @return string
     */
    public function message(){

        return trans('helper::response.item_not_found');

    }
}
