<?php


namespace aqsat\helper\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model{

    protected $primaryKey = 'uuid';

    public $incrementing = false;


    /**
     * @param $data array
     * @return self
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @Description This function for create self
     */

    final static public function createSelf(array $data)
    {
        return self::create($data);
    }

    /**
     * @param $data array
     * @return self
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @Description This function for get data by custom query
     */

    final static public function getWhere(array $data){

        return self::getWhere($data)->get();

    }

    /**
     * @param $data array
     * @return self
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @Description This function for find row by custom query
     */

    final static public function findWhere(array $data){

        return self::where($data)->first();

    }

    /**
     * @param $data array
     * @return self
     * @author Anas almasri (anas.almasri@hayperpay.com)
     * @Description This function for get data by custom query - pagination
     */

    final static public function getPaginate(array $data , $limit = 30 , $with = [] , $order_by = "uuid" , $sort = "DESC"){

        return self::where($data)->with($with)->orderBy($order_by , $sort)->paginate($limit);

    }


}
