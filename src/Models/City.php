<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: City model
 */



namespace aqsat\helper\Models;

use aqsat\helper\Observers\CityObserver;

/**
 * @property string $uuid
 * @property string $country_uuid
 * @property string $name
 * @property int $order
 * @property boolean $status
 */

class City extends BaseModel {

    public static function boot()
    {
        parent::boot();

        self::observe(CityObserver::class);
    }

    protected $fillable = [

        'uuid',
        'country_uuid',
        'name',
        'order',
        'status'
    ];


}
