<?php

/*
 * @author Anas almasri (anas.almasri@hayperpay.com)
 * @Description: Country model
 */



namespace aqsat\helper\Models;

use aqsat\helper\Observers\CountryObserver;
use aqsat\owners\Models\OwnerCountry;

/**
 * @property string $uuid
 * @property string $title
 * @property string $iso2
 * @property string $iso3
 * @property int $order
 * @property boolean $status
 */

class Country extends BaseModel {

    public static function boot()
    {
        parent::boot();

        self::observe(CountryObserver::class);
    }

    protected $fillable = [

        'uuid',
        'name',
        'slug',
        'iso2',
        'iso3',
        'order',
        'status'
    ];

    /**
     * Has many relations
     */

    public function OwnerCountry()
    {
        return $this->hasMany(OwnerCountry::class, 'country_id');
    }

}
