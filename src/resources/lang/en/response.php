<?php


return [
    'data_retrieved_successfully'=>'Data retrieved successfully',
    'invalid_identity_number'=>"Invalid identity number",
    'invalid_phone_format'=>'Invalid phone format.',
    'item_not_found'=>'Item not found.',
    'item_already_found'=>'Item already found.',


];
