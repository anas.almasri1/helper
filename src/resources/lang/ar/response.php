<?php


return [

    'data_retrieved_successfully'=>'تم استرجاع البيانات بنجاح.',
    'invalid_identity_number'=>'رقم المعرف غير صحيح .',
    'invalid_phone_format'=>'صيغه الهاتف غير صحيحه.',
    'item_not_found'=>'العنصر غير موجود.',
    'data_created_successfully'=>'Data created successfully',
    'item_already_found'=>'العنصر موجود بالفعل .',

];
